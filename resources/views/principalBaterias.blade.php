@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma: 2D3 - Grupo 2</h1>
        <h2>Participantes:</h2>
        <hr>  

        <table class="table table-striped table-bordered">
            <tr>
                <th>Matricula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <th>0072551</th>
                <th>Fernanda</th>
                <th>criação do programa</th>
            </tr>
            <tr>
                <th>0072528</th>
                <th>Ingrid</th>
                <th>calcular as baterias</th>
            </tr>
            <tr>
                <th>0072525</th>
                <th>Isadora</th>
                <th>criação do programa</th>
            </tr>
            <tr>
                <th>0073006</th>
                <th>Maíra</th>
                <th>calcular as baterias</th>
            </tr>
            <tr>
                <th>0072549</th>
                <th>Taís</th>
                <th>gerente</th>
            </tr>
        </table>
        <img class="grupo" src="imgs/grupo.webp"grupo">
    </main>

    @endsection

    @section('rodape')
    <h4>Rodapé da pagina inicial</h4>

    @endsection
    
