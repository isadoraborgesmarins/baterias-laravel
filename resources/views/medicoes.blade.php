
@extends('templates.base')

@section('conteudo')
    <main>

        <h1>Mediçoes:</h1>
        <hr>
        <h2>Valores obtidos</h2>
        <table class="table table-striped table-bordered"id="tbDados">
            <thead>
            <tr>
        
                <th>Pilha/Bateria</th>
                <th>Tensao nominal</th>
                <th>Capacidade de corrente</th>
                <th>Tensao sem carga</th>
                <th>Tensao com carga</th>
                <th>Resistencia de carga(ohm)</th>
 

                <th>Resistencia interna(ohm)</th>
            </tr>
        </thead>

        <tbody>
            @foreach($medicoes as $medicao)
            <tr>
                <td>{{$medicao->pilha_bateria}}</td>
                <td>{{number_format($medicao->tensao_nominal,1 ,'.','')}}</td>
                <td>{{$medicao->capacidade_correnete}}</td>
                <td>{{$medicao->tensao_sem_carga}}</td>
                <td>{{$medicao->tensao_com_carga}}</td>
                <td>{{$medicao->resistencia_carga}}</td>
                <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td

            </tr>
            @endforeach
        </tbody>

        </table>

        
    </main>
    
    @endsection

    @section('rodape')
    <h4>Rodapé medições</h4>

    @endsection